import React, {Component} from 'react';

class CurrencyInput extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      amount: ''
    };
  }

  handleChange (event) {
    this.props.changeAmt(event.target.value)
  }

  render() {
    return(
      <section>
        <label>{this.props.currency}</label>
        <input
          type="text"
          onChange={this.handleChange}
          value={this.props.amt}
        />
      </section>
    )
  }
}

export default CurrencyInput;
import React, {Component} from 'react';
import CurrencyInput from "./CurrencyInput";

class CurrencyTranslator extends Component {

  constructor(props){
    super(props);
    this.state = {
      amtUSD: '',
      amtCNY: ''
    }
    this.changeAmtCNY = this.changeAmtCNY.bind(this);
    this.changeAmtUSD = this.changeAmtUSD.bind(this);
  }

  changeAmtUSD(amt){
    this.setState({amtUSD: amt / 6, amtCNY: amt});
    setTimeout( () => {
      console.log(amt);
      console.log(this.state.amtCNY + "CNY-NEW");
      console.log(this.state.amtUSD + "USD-NEW");
    })
  }

  changeAmtCNY(amt){
    this.setState({amtCNY: amt * 6, amtUSD: amt}, () => {
      console.log('callback', this.state);
    });

    // setTimeout(() => {
      console.log('...', this.state);

    //   console.log(this.state.amtUSD + "USD-NEW");
    //   console.log(this.state.amtCNY + "CNY-NEW");
    // }, 1000);
  }



  render() {
    return (
      <div>
        <CurrencyInput currency="USD" changeAmt = {this.changeAmtCNY} amt={this.state.amtUSD}/>
        <CurrencyInput currency="CNY" changeAmt = {this.changeAmtUSD} amt={this.state.amtCNY}/>
      </div>
    )
  }

}

export default CurrencyTranslator;